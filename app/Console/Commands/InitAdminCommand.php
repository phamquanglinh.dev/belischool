<?php

namespace App\Console\Commands;

use App\Models\User;
use App\ObjectHelper\Table;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class InitAdminCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('Khởi tạo tài khoản admin');
        DB::table(Table::$usersTable)->updateOrInsert([
            'id' => 1
        ], [
            'id' => 1,
            'name' => 'Administrator',
            'username' => 'belischool',
            'email' => 'hughnguyen@belischool.com',
            'password' => Hash::make('bsm123@'),
            'phone' => '00000000',
            'role' => User::ADMIN_ROLE,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'verified' => 1
        ]);
        $this->info('Khởi tạo tài khoản admin => Thành công');
    }
}
