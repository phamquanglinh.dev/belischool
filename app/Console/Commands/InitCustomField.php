<?php

namespace App\Console\Commands;

use App\Models\CustomField;
use App\ObjectHelper\Table;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class InitCustomField extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init:custom_fields';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::table(Table::$customFieldStructsTable)->truncate();
        DB::table(Table::$customFieldStructsTable)->insert([
            [
                'name' => 't.gender',
                'type' => CustomField::TYPE_SELECT,
                'description' => 'Giới tính',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'teacher'
            ],
            [
                'name' => 't.description',
                'type' => CustomField::TYPE_TEXTAREA,
                'description' => 'Giới thiệu',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'teacher'
            ],
            [
                'name' => 't.main_color',
                'type' => CustomField::TYPE_SELECT,
                'description' => 'Màu giao diện',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'teacher'
            ],
            [
                'name' => 't.school',
                'type' => CustomField::TYPE_TEXT,
                'description' => 'Trường học/Cơ quan',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'teacher'
            ],
            [
                'name' => 't.birthday',
                'type' => CustomField::TYPE_DATE,
                'description' => 'Ngày sinh',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'teacher'
            ],
            [
                'name' => 't.ward',
                'type' => CustomField::TYPE_SELECT,
                'description' => 'Phường/Xã',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'teacher'
            ],
            [
                'name' => 't.district',
                'type' => CustomField::TYPE_SELECT,
                'description' => 'Quận/Huyện',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'teacher'
            ],
            [
                'name' => 't.skills',
                'type' => CustomField::TYPE_SELECT,
                'description' => 'Chuyên môn',
                'is_multiple' => 1,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'teacher'
            ],
            [
                'name' => 's.gender',
                'type' => CustomField::TYPE_SELECT,
                'description' => 'Giới tính',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
            [
                'name' => 's.description',
                'type' => CustomField::TYPE_TEXTAREA,
                'description' => 'Giới thiệu',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
            [
                'name' => 's.main_color',
                'type' => CustomField::TYPE_SELECT,
                'description' => 'Màu giao diện',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
            [
                'name' => 's.school',
                'type' => CustomField::TYPE_TEXT,
                'description' => 'Trường học/Cơ quan',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
            [
                'name' => 's.birthday',
                'type' => CustomField::TYPE_DATE,
                'description' => 'Ngày sinh',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
            [
                'name' => 's.ward',
                'type' => CustomField::TYPE_SELECT,
                'description' => 'Phường/Xã',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
            [
                'name' => 's.district',
                'type' => CustomField::TYPE_SELECT,
                'description' => 'Quận/Huyện',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
            [
                'name' => 's.father_name',
                'type' => CustomField::TYPE_TEXT,
                'description' => 'Tên bố',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
            [
                'name' => 's.mother_name',
                'type' => CustomField::TYPE_TEXT,
                'description' => 'Tên mẹ',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
            [
                'name' => 's.father_phone',
                'type' => CustomField::TYPE_TEXT,
                'description' => 'Số điện thoại bố',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
            [
                'name' => 's.mother_phone',
                'type' => CustomField::TYPE_TEXT,
                'description' => 'Số điện thoại mẹ',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
            [
                'name' => 's.father_email',
                'type' => CustomField::TYPE_TEXT,
                'description' => 'Email bố',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
            [
                'name' => 's.mother_email',
                'type' => CustomField::TYPE_TEXT,
                'description' => 'Email mẹ',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
            [
                'name' => 's.father_agency',
                'type' => CustomField::TYPE_TEXT,
                'description' => 'Cơ quan làm việc của bố',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
            [
                'name' => 's.mother_agency',
                'type' => CustomField::TYPE_TEXT,
                'description' => 'Cơ quan làm việc của mẹ',
                'is_multiple' => 0,
                'is_required' => 0,
                'is_unique' => 0,
                'is_default' => 1,
                'group' => 'student'
            ],
        ]);
    }
}
