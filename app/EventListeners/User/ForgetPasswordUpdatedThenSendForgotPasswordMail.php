<?php

namespace App\EventListeners\User;

use App\Events\User\ForgetPasswordWasUpdated;
use Illuminate\Support\Facades\Mail;

class ForgetPasswordUpdatedThenSendForgotPasswordMail
{
    public function __construct()
    {
        // Constructor không cần tham số gì thêm ở đây
    }

    public function handle(ForgetPasswordWasUpdated $event): void
    {
        $user = $event->getUser();

        $method = $event->getMethod();

        if ($method !== 'email') {
            return;
        }

        $token = $user->getAttribute('forget_token');
        $name = $user->getAttribute('name');
        $forgetLink = url('/') . "/#/recover_password?token=" . $token;

        // Gửi email
        Mail::send('emails.forget_password_mail', [
            'name' => $name,
            'forgetLink' => $forgetLink,
        ], function ($message) use ($user) {
            $message->to($user->email)
                ->subject('Khôi phục mật khẩu tài khoản của bạn');
        });
    }
}
