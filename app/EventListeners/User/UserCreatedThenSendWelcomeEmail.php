<?php

namespace App\EventListeners\User;

use App\Events\User\UserCreated;
use App\Jobs\TriggerSendSystemEmail;
use App\Mail\WelcomeMail;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Mail;
use Microsoft\Graph\Exception\GraphException;

class UserCreatedThenSendWelcomeEmail
{
    public function __invoke(UserCreated $event): void
    {
        $user = $event->getUser();

        if (!$user->get('email')) {
            return;
        }

        $mailObject = new WelcomeMail($user);

        dispatch(new TriggerSendSystemEmail($user->{'email'}, $mailObject));
    }

}
