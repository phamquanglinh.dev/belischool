<?php

namespace App\EventListeners\User;

use App\Events\User\UserRequestVerified;
use App\Jobs\TriggerSendSystemEmail;
use App\Mail\VerifyOtpMail;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class UserRequestVerifiedThenSendVerifySmsOrEmail
{
    public function __invoke(UserRequestVerified $event)
    {
        $user = $event->getUser();

        if ($user->getAttribute('phone')) {
            $this->sendVerifySms($user->getAttribute('phone'));
        }

        if ($user->getAttribute('email')) {
            $this->sendVerifyEmail($user);
        }
    }

    private function sendVerifyEmail(User $user): void
    {
        $otp = $user->get('otp');

        $mailObject = new VerifyOtpMail($user, $otp);

        dispatch(new TriggerSendSystemEmail($user->get('email'),$mailObject));
    }

    private function sendVerifySms(string $phone)
    {
        // Logic gửi tin nhắn xác minh qua SMS (tùy chỉnh theo dịch vụ SMS của bạn)
    }
}
