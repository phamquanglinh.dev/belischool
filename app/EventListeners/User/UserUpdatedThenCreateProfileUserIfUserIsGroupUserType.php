<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 25/10/2024 5:01 pm
 */

namespace App\EventListeners\User;

use App\Events\User\UserUpdated;
use App\Models\User;
use App\ObjectHelper\QueryHelper;

class UserUpdatedThenCreateProfileUserIfUserIsGroupUserType
{
    public function __construct()
    {
    }

    /**
     * @throws \Throwable
     */
    public function __invoke(UserUpdated $event): void
    {
        $user = $event->getUser();

        if (!$user->getAttribute('is_group')) {
            return;
        }

        $userData = $user->attributesToArray();

        $accountId = $userData['id'];

        unset($userData['id']);

        $primaryData = array_intersect_key($userData, array_flip([
            'name',
            'email',
            'phone',
            'role',
            'avatar',
            'username',
            'verified',
            'custom_fields'
        ]));

        $primaryData['username'] = $primaryData['username'] . "_1";

        $primaryData['custom_fields'] = is_string($primaryData['custom_fields']) ? json_decode($primaryData['custom_fields'], 1) : $primaryData['custom_fields'];

        $primaryData['is_group'] = 0;

        $primaryData['account_id'] = $accountId;

        $mainProfile = new User($primaryData);

        $mainProfile->populate($primaryData);

        QueryHelper::handleCreate($mainProfile);

        $user->set('custom_fields', []);
    }
}
