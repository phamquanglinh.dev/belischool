<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 25/10/2024 5:10 pm
 */

namespace App\EventListeners\User;

use App\Events\User\UserUpdated;
use App\Models\User;
use App\ObjectHelper\Table;
use Illuminate\Support\Facades\DB;

class UserUpdatedThenSaveCustomFields
{
    public function __construct()
    {
    }

    public function __invoke(UserUpdated $event)
    {
        $user = $event->getUser();

        if (!$user->get('custom_fields')) {
            return;
        }

        $customFields = json_decode($user->get('custom_fields'), 1);

        $customFieldKeys = array_keys($customFields);

        $role = $user->get('role');

        if (!$role) {
            $role = $user->fetch('role');
        }

        $group = null;

        if ($role == User::TEACHER_ROLE) {
            $group = 'teacher';
        }

        if ($role == User::GROUP_ROLE || $role == User::STUDENT_ROLE) {
            $group = 'student';
        }

        $customFieldStructs = DB::table(Table::$customFieldStructsTable)
            ->where('group', $group)
            ->whereIn('name', $customFieldKeys)->get();

        foreach ($customFieldStructs as $customFieldStruct) {
            $record = (array)$customFieldStruct;

            $value = $customFields[$record['name']] ?? null;

            if ($record['is_multiple'] == 1) {
                $value = is_array($value) ? json_encode($value) : $value;
            }

            DB::table(Table::$customFieldValuesTable)->updateOrInsert([
                'custom_field_id' => $record['id'],
                'user_id' => $user->get('id'),
            ], [
                'custom_field_id' => $record['id'],
                'user_id' => $user->get('id'),
                'value' => $value
            ]);
        }
    }
}
