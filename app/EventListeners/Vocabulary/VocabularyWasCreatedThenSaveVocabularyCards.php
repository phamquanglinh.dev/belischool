<?php

namespace App\EventListeners\Vocabulary;

use App\Events\Vocabulary\VocabularyWasCreated;
use App\Factories\Contexts\VocabularyContext;
use App\ObjectHelper\Table;
use Illuminate\Support\Facades\DB;

class VocabularyWasCreatedThenSaveVocabularyCards
{
    public function __invoke(VocabularyWasCreated $event): void
    {
        $vocabulary = $event->getVocabulary();

        $vocabularyCards = $vocabulary->get('vocabulary_cards');

        /**
         * @var VocabularyContext $context
         */
        $context = $vocabulary->get('context');

        $vocabularyId = $vocabulary->get('id');

        foreach ($vocabularyCards as $vocabularyCard) {
            $vocabularyCard['vocabulary_id'] = $vocabularyId;
            $vocabularyCard['created_by'] = $context->getInteractiveUserId();
            DB::table(Table::$vocabularyCardTable)->insert($vocabularyCard);
        }
    }
}
