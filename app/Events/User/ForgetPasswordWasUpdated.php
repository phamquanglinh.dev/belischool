<?php

namespace App\Events\User;

use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ForgetPasswordWasUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private User $user;
    private string $method;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        User $user,
        string $method
    )
    {
        $this->user = $user;
        $this->method = $method;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
