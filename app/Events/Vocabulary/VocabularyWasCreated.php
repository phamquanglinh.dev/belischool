<?php

namespace App\Events\Vocabulary;

use App\Models\Vocabulary;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class VocabularyWasCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private Vocabulary $vocabulary;

    /**
     * Create a new event instance.
     */
    public function __construct(
        Vocabulary $vocabulary
    )
    {
        //
        $this->vocabulary = $vocabulary;
    }

    public function getVocabulary(): Vocabulary
    {
        return $this->vocabulary;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): array
    {
        return [
            new PrivateChannel('channel-name'),
        ];
    }
}
