<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 25/10/2024 3:26 pm
 */

namespace App\Factories;

use App\Exceptions\ValidationException;
use Illuminate\Support\Facades\Validator;

class BaseFactory
{
    /**
     * @param array $data
     * @param $rules
     *
     * @return \Illuminate\Contracts\Validation\Validator
     *
     * @throws ValidationException
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  25/10/2024 16:49:40
     */
    public function validateParams(array $data, $rules): \Illuminate\Contracts\Validation\Validator
    {
        $notification = Validator::make($data, $rules);

        if ($notification->fails()) {
            throw new ValidationException($notification->errors()->toArray());
        }

        return $notification;
    }
}
