<?php

namespace App\Factories\Contexts;

class BaseFactoryContext
{
    private int $interactiveUserId;
    private int $interactiveRole;

    public function __construct(
        int $interactiveUserId = 0,
        int $interactiveRole = 0
    )
    {
        $this->interactiveUserId = $interactiveUserId;
        $this->interactiveRole = $interactiveRole;
    }

    public function getInteractiveUserId(): int
    {
        return $this->interactiveUserId;
    }

    public function getInteractiveRole(): int
    {
        return $this->interactiveRole;
    }
}
