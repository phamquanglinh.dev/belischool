<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 06/09/2024 10:55 am
 */

namespace App\Factories;

use App\Models\AppNotification;
use App\Models\User;

class NotificationFactory
{
    public function makeNotification(array $notificationData, array $user): AppNotification
    {
        $notification = new AppNotification();

        $notification->populate($notificationData);

        switch ($notification->get('object_type')) {
            case AppNotification::TYPE_STUDENT:
                $this->makeStudentNotification($notification, $user);
                break;
            case AppNotification::TYPE_CARD:
                $this->makeCardNotification($notification, $user);
                break;
            default:
                $notification->set('title', '---');
                $notification->set('description', '---');
                break;
        }

        $notification->set('object_data', json_encode($notification->get('object_data')));

        return $notification;
    }

    private function makeStudentNotification(AppNotification $notification, array $user): void
    {
        $name = $user['name'] ?? "HT";
        $objectData = $notification->get('object_data');

        switch ($notification->get('action_type')) {
            case AppNotification::ACT_CREATE:
                $title = __('notification.act_create.student.title');
                $description = __('notification.act_create.student.description', [
                    'name' => $name,
                    'student_name' => $objectData['student_name']
                ]);

                break;
            case AppNotification::ACT_UPDATE:
                $title = __('notification.act_create.student.title');
                $description = __('notification.act_update.student.description', [
                    'name' => $name,
                    'student_name' => $objectData['name']
                ]);
                $notification->set('title', $title);
                $notification->set('description', $description);
                break;
            default:
                $title = '';
                $description = '';
                break;
        }

        $notification->set('title', $title);
        $notification->set('description', $description);
    }

    private function makeCardNotification(AppNotification $notification, array $user): void
    {
        $name = $user['name'] ?? "HT";
        $objectData = $notification->get('object_data');

        switch ($notification->get('action_type')) {
            case AppNotification::ACT_APPROVE:
                $title = __('notification.act_approve.card.title');
                $description = __('notification.act_approve.card.description', [
                    'name' => $name,
                    'uuid' => $objectData['uuid']
                ]);

                break;
            default:
                $title = '';
                $description = '';
                break;
        }

        $notification->set('title', $title);
        $notification->set('description', $description);
    }
}
