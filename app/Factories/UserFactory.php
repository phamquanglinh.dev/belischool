<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 25/10/2024 3:22 pm
 */

namespace App\Factories;

use App\Exceptions\ValidationException;
use App\Factories\Contexts\UserContext;
use App\Models\User;
use App\ObjectHelper\Table;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class UserFactory extends BaseFactory
{
    /**
     * @throws ValidationException
     */
    public function makeUser(UserContext $context, array $userData): User
    {
        $this->validateParams($userData, [
            'name' => 'string|required',
            'username' => 'string|required|unique:users,username',
            'email' => 'email|nullable',
            'password' => 'required|string',
            'plain_password' => 'required|string',
            'phone' => 'string|required',
            'avatar' => 'string|nullable',
            'role' => 'integer|nullable'
        ]);

        $user = new User();

        $user->populate($userData);

        $user->setIfEmpty('role', User::USER_ROLE);

        if ($user->get('password') !== $user->get('plain_password')) {
            throw new ValidationException([
                'plain_password' => __('validation.errors.plain_password.not_match')
            ]);
        }

        if ($this->isEasyPassword($userData['password'])) {
            throw new ValidationException([
                'password' => 'Mật khẩu phải có ít nhất 6 ký tự, bao gồm chữ hoa, chữ thường và ký tự đặc biệt.',
            ]);
        }

        $user->set('password', Hash::make($user->get('password')));

        $user->set('is_group', 1);

        return $user;
    }

    /**
     * @throws ValidationException
     */
    public function modifyUser(UserContext $userContext, User $user, array $userData): User
    {
        $this->validateParams($userData, [
            'name' => 'string|required',
            'email' => 'email|nullable',
            'new_password' => 'nullable|string',
            'old_password' => 'nullable|string',
            'plain_password' => 'nullable|string',
            'phone' => 'string|nullable',
            'avatar' => 'string|nullable',
            'role' => 'integer|nullable|in:1,2,3',
            'custom_fields' => 'array|nullable'
        ]);

        $user->populate($userData);

        if ($user->isDirty('new_password')) {
            if ($user->isClean(['old_password'])) {
                throw new ValidationException([
                    'old_password' => 'Required Old Password',
                ]);
            }
            if ($user->isClean(['old_password'])) {
                throw new ValidationException([
                    'plain_password' => 'Required Plain Password',
                ]);
            }

            if ($user->get('new_password') !== $user->get('plain_password')) {
                throw new BadRequestException(__('validation.errors.plain_password.not_match'));
            }

            if (!Hash::check($user->get('old_password'), $user->get('password'))) {
                throw new BadRequestException(__('validation.errors.old_password.not_match'));
            }

            $user->set('password', Hash::make($user->get('new_password')));
        }

        return $user;
    }

    public function isEasyPassword(string $password): bool
    {
        // Kiểm tra độ dài
        if (strlen($password) < 6) {
            return true;
        }

        // Kiểm tra có chứa chữ cái viết hoa, viết thường và ký tự đặc biệt
        if (!preg_match('/[A-Z]/', $password) ||  // Chữ cái viết hoa
            !preg_match('/[a-z]/', $password) ||  // Chữ cái viết thường
            !preg_match('/[^a-zA-Z\d]/', $password)) {  // Ký tự đặc biệt
            return true;
        }

        return false;
    }

    /**
     * @throws ValidationException
     */
    public function prepareForgotPassword(UserContext $context, User $user, array $input): User
    {
        $this->validateParams($input, [
            'new_password' => 'required|string',
            'plain_password' => 'required|string'
        ]);

        $user->populate($input);

        if ($user->getAttribute('new_password')) {
            if ($user->isDirty(['old_password'])) {
                if (!Hash::check($user->get('old_password'), $user->get('password'))) {
                    throw new BadRequestException(__('validation.errors.old_password.not_match'));
                }
            }

            if ($this->isEasyPassword($user->get('new_password'))) {
                throw new ValidationException([
                    'new_password' => 'Mật khẩu phải có ít nhất 6 ký tự, bao gồm chữ hoa, chữ thường và ký tự đặc biệt.',
                ]);
            }

            if ($user->get('new_password') !== $user->get('plain_password')) {
                throw new ValidationException([
                    'plain_password' => __('validation.errors.plain_password.not_match'),
                ]);
            }

            $user->set('password', Hash::make($user->get('new_password')));
        }

        DB::table(Table::$passwordResetTable)->where('username', $user->getAttribute('username'))->delete();

        return $user;
    }

    public function prepareVerifyUser(User $user): User
    {
        $user->set('verified', 1);

        DB::table(Table::$otp)->where('username', $user->get('username'))->delete();

        return $user;
    }
}
