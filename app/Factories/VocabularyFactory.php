<?php

namespace App\Factories;

use App\Exceptions\ValidationException;
use App\Factories\Contexts\VocabularyContext;
use App\Models\Vocabulary;

class VocabularyFactory extends BaseFactory
{
    /**
     * @throws ValidationException
     */
    public function makeVocabulary(VocabularyContext $context, array $vocabularyData): Vocabulary
    {
        $this->validateParams($vocabularyData, [
            'name' => 'required|string',
            'vocabulary_cards' => 'required|array',
            'vocabulary_cards.*.name' => 'string|required',
            'vocabulary_cards.*.order' => 'integer|required',
            'vocabulary_cards.*.translated' => 'string|required',
            'vocabulary_cards.*.system_voice' => 'string|required',
            'vocabulary_cards.*.teacher_voice' => 'string|nullable',
            'vocabulary_cards.*.teacher_image' => 'string|required',
        ]);

        $vocabulary = new Vocabulary();

        $vocabulary->populate($vocabularyData);

        $vocabulary->set('created_by', $context->getInteractiveUserId());

        $vocabulary->set('context', $context);

        return $vocabulary;
    }
}
