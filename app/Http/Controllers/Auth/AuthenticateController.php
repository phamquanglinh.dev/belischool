<?php

namespace App\Http\Controllers\Auth;

use App\Events\User\ForgetPasswordWasUpdated;
use App\Events\User\UserRequestVerified;
use App\Exceptions\NotFoundException;
use App\Exceptions\ValidationException;
use App\Factories\Contexts\UserContext;
use App\Factories\UserFactory;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\ObjectHelper\AuthHelper;
use App\ObjectHelper\HttpResponse;
use App\ObjectHelper\QueryHelper;
use App\ObjectHelper\Table;
use App\ObjectHelper\UserQueryHandler;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

use function Symfony\Component\String\u;

class AuthenticateController extends Controller
{
    /**
     * @param Request $request
     * @param HttpResponse $httpResponse
     * @param UserFactory $userFactory
     *
     * @return JsonResponse
     *
     * @throws ValidationException
     * @throws \Throwable
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  25/10/2024 15:37:20
     */
    public function registerAction(Request $request, HttpResponse $httpResponse, UserFactory $userFactory): JsonResponse
    {
        $input = $request->input();

        $user = $userFactory->makeUser(new UserContext(), $input);

        $user = QueryHelper::handleCreate($user);

        return $httpResponse->responseDataCreated($user->{'id'});
    }

    /**
     * @throws ValidationException
     * @throws NotFoundException
     */
    public function loginAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'username' => 'required|string',
            'password' => 'required|string'
        ]);

        if ($notification->fails()) {
            throw new ValidationException($notification->errors()->toArray());
        }

        $user = QueryHelper::handleGetOne(User::class, new UserQueryHandler([
            'where' => [
                'username:eq' => $input['username']
            ]
        ]));

        if (! $user) {
            throw new NotFoundException();
        }

        if (! Hash::check($input['password'], $user->getPassword())) {
            throw new BadRequestException(__('validation.errors.password.wrong_password'));
        }

        $token = AuthHelper::encryptString(json_encode([
            'id' => $user->id(),
            'name' => $user->name(),
            'is_student_account' => $user->get('is_student_account'),
            'phone' => $user->get('phone'),
            'role' => $user->role(),
            'avatar' => $user->get('avatar'),
            'username' => $user->get('username'),
            'verified' => $user->get('verified')
        ]));

        return $httpResponse->responseData([
            'data' => [
                'token' => $token
            ]
        ]);
    }

    /**
     * @param HttpResponse $httpResponse
     *
     * @return JsonResponse
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  25/10/2024 16:32:22
     */
    public function getCurrentUserInfoAction(HttpResponse $httpResponse): JsonResponse
    {
        $currentUser = $this->authenticationService->getCurrentUser();

        $user = QueryHelper::find(User::class, $currentUser->id(), $currentUser->id());

        return $httpResponse->responseDataModel($user);
    }

    public function getCurrentUserAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $currentUser = $this->authenticationService->getCurrentUser();

        $user = QueryHelper::find(User::class, $currentUser->id(), $currentUser->id());

        return $httpResponse->responseDataModel($user);
    }

    public function findAccountAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'username' => 'required'
        ]);

        if ($notification->fails()) {
            throw new ValidationException($notification->errors()->toArray());
        }

        $user = QueryHelper::handleGetOne(
            User::class, new UserQueryHandler(
                [
                    'where' => [
                        'username' => $input['username']
                    ],
                    'fields' => 'name,email,phone,username'
                ]
            )
        );

        if (! $user) {
            throw new NotFoundException();
        }

        return $httpResponse->responseDataModel($user);
    }

    /**
     * @param Request $request
     * @param HttpResponse $httpResponse
     * @return JsonResponse
     * @throws NotFoundException
     * @throws ValidationException
     */
    public function forgotPasswordAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'username' => 'required|string',
            'method' => 'required|string|in:phone,email'
        ]);

        if ($notification->fails()) {
            throw new ValidationException($notification->errors()->toArray());
        }
        $user = QueryHelper::handleGetOne(User::class, new UserQueryHandler([
            'where' => [
                'username:eq' => $input['username']
            ]
        ], User::class));

        if (! $user) {
            throw new NotFoundException();
        }

        if ($input['method'] == 'email') {
            if (! $user->get('email')) {
                throw new NotFoundException();
            }
        }

        if ($input['method'] == 'phone') {
            if (! $user->get('phone')) {
                throw new NotFoundException();
            }
        }

        $token = Str::random(30);

        $exist = DB::table('password_resets')->where('username', $input['username'])->first();

        if ($exist) {
            if (((int)(($exist->{'expired_in'} ?? 0) + 600)) < Carbon::now()->timestamp) {
                $user->setAttribute('forget_token', $token);

                event(new ForgetPasswordWasUpdated($user));

                return $httpResponse->responseData([
                    'message' => __('response.update.succeed')
                ]);
            }
        }

        DB::table(Table::$passwordResetTable)->updateOrInsert([
            'username' => $user->get('username'),
        ], [
            'username' => $user->get('username'),
            'token' => $token,
            'expired_in' => Carbon::now()->timestamp + 3600
        ]);

        $user->setAttribute('forget_token', $token);

        event(new ForgetPasswordWasUpdated($user, $input['method']));

        return $httpResponse->responseData([
            'message' => __('response.update.succeed')
        ]);
    }

    /**
     * @param Request $request
     * @param HttpResponse $httpResponse
     * @param UserFactory $userFactory
     * @return JsonResponse
     * @throws NotFoundException
     * @throws ValidationException
     */
    public function recoverPasswordAction(Request $request, HttpResponse $httpResponse, UserFactory $userFactory)
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'token' => 'required|string',
        ]);

        if ($notification->fails()) {
            throw new ValidationException($notification->errors()->toArray());
        }

        $token = DB::table(Table::$passwordResetTable)->where('token', $input['token'])->first();

        if (! $token) {
            throw new NotFoundException();
        }

        if ($token->{'expired_in'} < Carbon::now()->timestamp) {
            throw new BadRequestException(__('response.expired.token'));
        }

        $user = QueryHelper::handleGetOne(User::class, new UserQueryHandler([
            'where' => [
                'username:eq' => $token->{'username'}
            ]
        ]));

        if (! $user) {
            throw new NotFoundException();
        }

        unset($input['token']);

        $user = $userFactory->prepareForgotPassword(new UserContext(), $user, $input);

        QueryHelper::handleUpdate($user);

        return $httpResponse->responseDataUpdated($user->{'id'});
    }

    /**
     * @throws ValidationException
     */
    public function requireVerifiedAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $currentUser = $this->authenticationService->getCurrentUser();

        $user = QueryHelper::find(User::class, $currentUser->id(), $currentUser->id());

        $notification = Validator::make($input, [
            'method' => 'required|string|in:sms,email'
        ]);

        if ($notification->fails()) {
            throw new ValidationException($notification->errors()->toArray());
        }

        $otp = rand(100000, 999999);

        DB::table(Table::$otp)->insert([
            'username' => $user->get('username'),
            'otp' => $otp,
            'expired_in' => Carbon::now()->timestamp + 3600
        ]);

        $user->set('otp', $otp);

        event(new UserRequestVerified($user));

        return $httpResponse->responseData([
            'message' => __('response.update.succeed')
        ]);
    }

    /**
     * @throws ValidationException
     * @throws NotFoundException
     */
    public function userVerifyAction(Request $request, HttpResponse $httpResponse, UserFactory $userFactory)
    {
        $currentUser = $this->authenticationService->getCurrentUser();

        $user = QueryHelper::find(User::class, $currentUser->id(), $currentUser->id());

        $input = $request->input();

        $notification = Validator::make($input, [
            'otp' => 'required|numeric|min:100000|max:999999'
        ]);

        if ($notification->fails()) {
            throw new ValidationException($notification->errors()->toArray());
        }

        $trueOtp = DB::table(Table::$otp)->where([
            'username' => $user->get('username'),
            'otp' => $input['otp'],
        ])->first();

        if (! $trueOtp) {
            throw new NotFoundException();
        }

        if ($trueOtp->{'expired_in'} < Carbon::now()->timestamp) {
            throw new BadRequestException('OTP đã hết hạn');
        }

        $user = $userFactory->prepareVerifyUser($user);

        QueryHelper::handleUpdate($user);

        return $httpResponse->responseDataUpdated($user->{'id'});
    }
}
