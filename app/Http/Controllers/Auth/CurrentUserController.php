<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\ForbiddenException;
use App\Exceptions\NotFoundException;
use App\Exceptions\ValidationException;
use App\Factories\Contexts\UserContext;
use App\Factories\UserFactory;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\ObjectHelper\AuthHelper;
use App\ObjectHelper\HttpResponse;
use App\ObjectHelper\QueryHelper;
use App\ObjectHelper\UserQueryHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CurrentUserController extends Controller
{
    /**
     * @param Request $request
     * @param HttpResponse $httpResponse
     * @param UserFactory $userFactory
     * @return JsonResponse
     * @throws ValidationException
     */
    public function updateCurrentUserAction(Request $request, HttpResponse $httpResponse, UserFactory $userFactory)
    {
        $currentUser = $this->authenticationService->getCurrentUser();

        $user = QueryHelper::find(User::class, $currentUser->id(), $currentUser->id());

        $input = $request->input();

        $user = $userFactory->modifyUser(new UserContext($currentUser->id(), $currentUser->role()), $user, $input);

        QueryHelper::handleUpdate($user);

        return $httpResponse->responseDataUpdated($user->{'id'});
    }

    public function getAllUserProfilesAction(Request $request, HttpResponse $httpResponse)
    {
        $currentUser = $this->authenticationService->getCurrentUser();

        $user = QueryHelper::find(User::class, $currentUser->id(), $currentUser->id());

        $userQueryHandler = new UserQueryHandler($request->input(), User::class);

        $userQueryHandler->setWhere([
            'account_id' => $user->id()
        ]);

        $profiles = QueryHelper::handleGetAll(User::class, $userQueryHandler);

        $count = QueryHelper::handleCount(User::class, $userQueryHandler);

        return $httpResponse->responseModelCollection($profiles, $count);
    }

    /**
     * @throws NotFoundException
     */
    public function selectProfileAction(Request $request, HttpResponse $httpResponse, int $id)
    {
        $currentUser = $this->authenticationService->getCurrentUser();

        $user = QueryHelper::find(User::class, $currentUser->id(), $currentUser->id());

        $userQueryHandler = new UserQueryHandler($request->input(), User::class);

        $userQueryHandler->setWhere([
            'account_id' => $user->id(),
            'id' => $id
        ]);

        $profile = QueryHelper::handleGetOne(User::class, $userQueryHandler);

        if (!$profile) {
            throw new NotFoundException();
        }

        $token = AuthHelper::encryptString(json_encode([
            'id' => $profile->id(),
            'name' => $profile->name(),
            'is_student_account' => $profile->get('is_student_account'),
            'phone' => $profile->get('phone'),
            'role' => $profile->role(),
            'avatar' => $profile->get('avatar'),
            'username' => $profile->get('username'),
            'verified' => $profile->get('verified')
        ]));

        return $httpResponse->responseData([
            'token' => $token
        ]);
    }

    /**
     * @param Request $request
     * @param HttpResponse $httpResponse
     * @param UserFactory $userFactory
     * @return JsonResponse
     * @throws ValidationException
     * @throws \Throwable
     */
    public function createUserProfilesAction(Request $request, HttpResponse $httpResponse, UserFactory $userFactory)
    {
        $currentUser = $this->authenticationService->getCurrentUser();

        $user = QueryHelper::find(User::class, $currentUser->id(), $currentUser->id());

        if (! $user->get('is_student_account')) {
            throw new ForbiddenException('Loại tài khoản không hợp lệ');
        }

        $input = $request->input();

        $input['account_id'] = $user->id();

        $input['role'] = User::STUDENT_ROLE;

        $input['is_student_account'] = 0;

        $input['verified'] = 1;

        $password = Hash::make(Str::random());

        $input['password'] = $password;

        $input['plain_password'] = $password;

        $profile = $userFactory->makeUser(new UserContext($user->id(), $user->id()), $input);

        $profile = QueryHelper::handleCreate($profile);

        return $httpResponse->responseDataCreated($profile->get('id'));
    }
}
