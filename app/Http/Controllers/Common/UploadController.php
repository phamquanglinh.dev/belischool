<?php

namespace App\Http\Controllers\Common;

use App\Exceptions\NotFoundException;
use App\Http\Controllers\Controller;
use App\Services\AuthenticationService;
use App\Services\DropboxService;
use App\Services\ResourceAbilitiesService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    private DropboxService $dropboxService;

    public function __construct(
        ResourceAbilitiesService $resourceAbilitiesService,
        AuthenticationService $authenticationService,
        DropboxService $dropboxService
    )
    {
        parent::__construct($resourceAbilitiesService, $authenticationService);

        $this->dropboxService = $dropboxService;
    }

    /**
     */
    public function uploadActionViaDropbox(Request $request)
    {
        $file = $request->file('file');
        $filePath = '/belischool/' . $file->getClientOriginalName();
        $fileContents = file_get_contents($file->getRealPath());

        $response = $this->dropboxService->uploadFile($filePath, $fileContents);

        return response()->json($response);
    }

    /**
     * @param string $path
     *
     * @return mixed
     *
     * @throws GuzzleException
     * @since:  08/11/2024 11:23:01
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     */
    public function getFileActionViaDropbox(string $path)
    {
        try {
            // Lấy file từ Dropbox
            $file = $this->dropboxService->getFile("belischool/" . $path);

            // Đoán loại MIME từ đuôi file nếu Content-Type không được cung cấp
            $mimeType = $file['type'] ?? mime_content_type($file['name']);

            // Nếu MIME type không được xác định, mặc định là 'application/octet-stream'
            if (!$mimeType || $mimeType === 'application/octet-stream') {
                $extension = pathinfo($file['name'], PATHINFO_EXTENSION);

                // Gán MIME type dựa trên phần mở rộng file
                $mimeType = match ($extension) {
                    'pdf' => 'application/pdf',
                    'jpg', 'jpeg' => 'image/jpeg',
                    'png' => 'image/png',
                    'gif' => 'image/gif',
                    'txt' => 'text/plain',
                    'html' => 'text/html',
                    'css' => 'text/css',
                    'js' => 'application/javascript',
                    'json' => 'application/json',
                    'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    default => 'application/octet-stream'
                };
            }

            // Kiểm tra nếu file là .xlsx thì render dưới dạng Google Docs Viewer
            if ($mimeType === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                $directoryPath = storage_path('app/public/belischool');

                if (!is_dir($directoryPath)) {
                    // Nếu thư mục không tồn tại, tạo thư mục
                    mkdir($directoryPath, 0755, true);
                }

                // Lưu file vào thư mục storage/belischool
                $tempPath = $directoryPath . '/' . $path;

                file_put_contents($tempPath, $file['contents']);

                // Tạo URL công khai cho file (dùng domain ảo)
                $fileUrl = asset('storage/belischool/' . $path);

                // Tạo URL Google Docs Viewer
                $googleViewerUrl = "https://docs.google.com/viewer?url={$fileUrl}&embedded=true";

                // Xóa file tạm sau khi sử dụng
//                unlink($tempPath);

                // Redirect đến Google Docs Viewer
                return redirect($googleViewerUrl);
            }

            // Trả về file với header thích hợp cho các loại file khác
            return response($file['contents'], 200)
                ->header('Content-Type', $mimeType)
                ->header('Content-Disposition', 'inline; filename="' . $file['name'] . '"')
                ->header('Content-Length', strlen($file['contents']));

        } catch (\Exception $e) {
            // Trả về lỗi nếu có
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

}
