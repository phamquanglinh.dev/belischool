<?php

namespace App\Http\Controllers;

use App\Services\AuthenticationService;
use App\Services\ResourceAbilitiesService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public ResourceAbilitiesService $resourceAbilitiesService;
    public AuthenticationService $authenticationService;

    public function __construct(
        ResourceAbilitiesService $resourceAbilitiesService,
        AuthenticationService $authenticationService,
    ) {
        $this->resourceAbilitiesService = $resourceAbilitiesService;
        $this->authenticationService = $authenticationService;
    }
}
