<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    // Hàm upload file lên S3
    public function upload(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file');

            $path = $file->store('uploads', 's3');

            $url = Storage::disk('s3')->url($path);

            return response()->json([
                'message' => 'Upload thành công!',
                'file_path' => $path,
                'file_url' => $url,
            ], 200);
        }

        return response()->json([
            'message' => 'Không có file nào được upload!',
        ], 400);
    }

    // Hàm lấy file từ S3
    public function getFile($filename)
    {
        // Kiểm tra file có tồn tại không
        if (Storage::disk('s3')->exists("uploads/{$filename}")) {
            // Lấy URL của file
            $url = Storage::disk('s3')->url("uploads/{$filename}");

            return response()->json([
                'message' => 'Lấy file thành công!',
                'file_url' => $url,
            ], 200);
        }

        return response()->json([
            'message' => 'File không tồn tại!',
        ], 404);
    }

    // Hàm download file từ S3
    public function downloadFile($filename)
    {
        // Kiểm tra file có tồn tại không
        if (Storage::disk('s3')->exists("uploads/{$filename}")) {
            $file = Storage::disk('s3')->get("uploads/{$filename}");
            $headers = [
                'Content-Type' => 'application/octet-stream',
                'Content-Disposition' => "attachment; filename={$filename}",
            ];

            return response($file, 200, $headers);
        }

        return response()->json([
            'message' => 'File không tồn tại!',
        ], 404);
    }
}
