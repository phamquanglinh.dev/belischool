<?php

namespace App\Http\Controllers\Vocabulary;

use App\Exceptions\ForbiddenException;
use App\Exceptions\ValidationException;
use App\Factories\Contexts\VocabularyContext;
use App\Factories\VocabularyFactory;
use App\Http\Controllers\Controller;
use App\Models\Vocabulary;
use App\ObjectHelper\HttpResponse;
use App\ObjectHelper\QueryHelper;
use App\ObjectHelper\UserQueryHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class VocabularyController extends Controller
{
    /**
     * @param Request $request
     * @param HttpResponse $httpResponse
     * @param VocabularyFactory $vocabularyFactory
     * @return JsonResponse
     * @throws ForbiddenException
     * @throws ValidationException
     * @throws \Throwable
     */
    public function createVocabularyAction(Request $request, HttpResponse $httpResponse, VocabularyFactory $vocabularyFactory)
    {
        $currentUser = $this->authenticationService->getCurrentUser();

        if (! $this->resourceAbilitiesService->canCreateVocabulary($currentUser->id())) {
            throw new ForbiddenException();
        }

        $vocabulary = $vocabularyFactory->makeVocabulary(new VocabularyContext($currentUser->id(), $currentUser->role()), $request->input());

        $vocabulary = QueryHelper::handleCreate($vocabulary);

        return $httpResponse->responseDataCreated($vocabulary->get('id'));
    }

    public function getAllVocabularyAction(Request $request, HttpResponse $httpResponse)
    {
        $currentUser = $this->authenticationService->getCurrentUser();

        $userQueryHandler = new UserQueryHandler($request->input(), Vocabulary::class);

        $profiles = QueryHelper::handleGetAll(Vocabulary::class, $userQueryHandler);

        $count = QueryHelper::handleCount(Vocabulary::class, $userQueryHandler);

        return $httpResponse->responseModelCollection($profiles, $count);
    }
}
