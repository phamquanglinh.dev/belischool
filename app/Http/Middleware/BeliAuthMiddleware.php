<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\ObjectHelper\AuthHelper;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class BeliAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse) $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->bearerToken();

        if (!$token) {
            throw new UnauthorizedException();
        }

        try {
            $decoded = json_decode(AuthHelper::decryptString($token));

            /**
             * @var User $user
             */
            $user = User::query()->find($decoded->id);

            if ( !$user) {
                throw new UnauthorizedException();
            }

            $request->user = $user;

        } catch (Exception $e) {
            throw new UnauthorizedException();
        }

        return $next($request);
    }
}
