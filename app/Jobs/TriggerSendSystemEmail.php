<?php

namespace App\Jobs;

use App\Mail\WelcomeMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class TriggerSendSystemEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private object $object_data;
    private string $email;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        string   $email,
        Mailable $object_data

    )
    {
        //
        $this->object_data = $object_data;
        $this->email = $email;
    }

    public function getObjectData(): object
    {
        return $this->object_data;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        Mail::to($this->email)->send($this->object_data);
    }
}
