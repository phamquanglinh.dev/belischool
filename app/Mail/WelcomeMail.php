<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeMail extends Mailable
{
    use Queueable, SerializesModels;

    public User $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function build(): WelcomeMail
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Chào mừng bạn đến với BeliSchool!')
            ->view('emails.welcome')
            ->with(['name' => $this->user->{'name'}]);
    }
}
