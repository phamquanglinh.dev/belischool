<?php

namespace App\Models;

use App\ObjectHelper\Table;
use App\ObjectHelper\UserQueryHandler;
use App\Traits\Entity;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use function Symfony\Component\String\s;

class AppNotification extends BaseModel
{
    use Entity;

    protected $table = 'notifications';

    protected $guarded = ['id'];

    public const ACT_CREATE = 'create';
    public const ACT_UPDATE = 'update';
    public const ACT_DELETE = 'delete';
    public const ACT_APPROVE = 'approve';
    public const ACT_SUBMIT = 'submit';
    public const ACT_CONFIRM = 'confirm';
    public const ACT_CONFIRM_ALL = 'confirm_all';
    public const ACT_COMMENT = 'comment';
    public const ACT_IMPORTED = 'imported';
    public const TYPE_STUDENT = 'student';
    public const TYPE_TEACHER = 'student';
    public const TYPE_SUPPORTER = 'student';
    public const TYPE_CARD = 'card';
    public const TYPE_CLASSROOM = 'student';
    public const TYPE_TRANSACTION = 'student';
    public const TYPE_SALARY_SETTING = 'student';
    public const TYPE_SALARY_SNAP = 'student';
    public const TYPE_SALARY_GROUP = 'student';

    public static function defineRetrieveFields(): array
    {
        return [
            'id',
            'title',
            'description',
            'object_id',
            'action_type',
            'object_type',
            'pin',
            'read',
        ];
    }

    public static function defineFillableFields(): array
    {
        return [

        ];
    }

    protected function handleFields(Builder &$builder, array $fields, UserQueryHandler $userQueryHandler): void
    {
        $userId = $userQueryHandler->getUserId();

        foreach ($fields as $field) {
            switch ($field) {
                case "pin":
                    $builder->selectSub(function ($query) use ($userId) {
                        $query->from(Table::$notificationUserTable)
                            ->select('pin')
                            ->whereColumn(Table::$notificationUserTable . '.notification_id', Table::$notificationTable . '.id')
                            ->where(Table::$notificationUserTable . '.user_id', $userId)
                            ->limit(1);
                    }, 'pin');
                    break;
                case "read":
                    $builder->selectSub(function ($query) use ($userId) {
                        $query->from(Table::$notificationUserTable)
                            ->select('read')
                            ->whereColumn(Table::$notificationUserTable . '.notification_id', Table::$notificationTable . '.id')
                            ->where(Table::$notificationUserTable . '.user_id', $userId)
                            ->limit(1);
                    }, 'read');
                    break;
                default:
                    $builder->addSelect($field);
                    break;
            }
        }
    }

    protected function handleInteractiveUser(Builder &$builder, UserQueryHandler $userQueryHandler, int $branchScope = 0): void
    {
        $builder->whereExists(function ($builder) use ($userQueryHandler) {
            $builder->selectRaw(DB::raw(1))->from(Table::$notificationUserTable)
                ->whereColumn(Table::$notificationTable . '.id', Table::$notificationUserTable . ".notification_id")
                ->where(Table::$notificationUserTable . '.user_id', $userQueryHandler->getUserId());
        });
    }
}
