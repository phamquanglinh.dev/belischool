<?php

namespace App\Models;

use App\ObjectHelper\UserQueryHandler;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public function populate(array $data = [])
    {
    }

    public function collectExternalFields(UserQueryHandler $userQueryHandler)
    {
    }

    public function handleCastData(UserQueryHandler $userQueryHandler): void
    {
        foreach (static::defineIntFields() ?? [] as $field) {
            $this->setAttribute($field, (int) $this->get($field));
        }
    }

    public function handleBuildQuery(UserQueryHandler $userQueryHandler)
    {
    }

    public function handleBuildQueryCount(UserQueryHandler $userQueryHandler, int $branchScope = 0)
    {
    }

    public static function defineRetrieveFields()
    {

    }

    public static function defineSaveableFields()
    {

    }

    public static function defineFillableFields()
    {

    }
    public static function defineIntFields()
    {

    }

    public function handlePrimaryData(array $data)
    {
    }

    public function get(string $key)
    {
        return $this->getAttribute($key);
    }

    public function set(string $key, mixed $value): void
    {
        $this->setAttribute($key, $value);
    }

    public function setIfEmpty(string $key, mixed $value): void
    {
        if (!$this->getAttribute($key)) {
            $this->setAttribute($key, $value);
        }
    }


}
