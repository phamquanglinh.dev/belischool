<?php

namespace App\Models;

use App\Traits\Entity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomField extends Model
{
    use Entity;
    public const TYPE_TEXT = 'text';
    public const TYPE_SELECT = 'select';
    public const TYPE_CHECKBOX = 'checkbox';
    public const TYPE_TEXTAREA = 'textarea';
    public const TYPE_DATE = 'date';
    public const TYPE_DATETIME = 'datetime';

    protected $casts = [
        'name' => 'string',
        'type' => 'string',
        'description' => 'string',
        'is_multiple' => 'integer',
        'is_required' => 'integer',
        'is_unique' => 'integer',
        'is_default' => 'integer',
    ];

    public static function defineFillableFields(): array
    {
        return [
            'name',
            'type',
            'description',
            'is_multiple',
            'is_required',
            'is_unique',
            'is_default',
            'custom_field_lists'
        ];
    }

    protected function externalFields(): array
    {
        return [
            'custom_field_lists'
        ];
    }


}
