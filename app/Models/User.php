<?php

namespace App\Models;

use App\ObjectHelper\Table;
use App\ObjectHelper\UserQueryHandler;
use App\Traits\Entity;
use Illuminate\Support\Facades\DB;

class User extends BaseModel
{
    use Entity;

    const USER_ROLE = 99;
    const GROUP_ROLE = 1;
    const STUDENT_ROLE = 2;
    const TEACHER_ROLE = 3;
    const ADMIN_ROLE = 0;

    protected $casts = [
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'pin' => 'integer',
        'is_group' => 'integer',
        'phone' => 'string',
        'role' => 'integer',
        'avatar' => 'string',
        'bcoin_token' => 'string',
        'custom_fields' => 'array',
        'username' => 'string',
        'verified' => 'integer',
        'account_id' => 'integer'
    ];

    public static function defineFillableFields(): array
    {
        return [
            'name',
            'email',
            'password',
            'role',
            'plain_password',
            'new_password',
            'pin',
            'phone',
            'avatar',
            'username',
            'custom_fields',
            'is_group',
            'account_id'
        ];
    }

    public static function defineRetrieveFields(): array
    {
        return [
            'id',
            'name',
            'email',
            'is_group',
            'phone',
            'role',
            'avatar',
            'username',
            'verified',
            'custom_fields',
            'account_id'
        ];
    }

    public static function defineSaveableFields(): array
    {
        return [
            'name',
            'email',
            'password',
            'pin',
            'is_group',
            'phone',
            'role',
            'avatar',
            'bcoin_token',
            'username',
            'account_id'
        ];
    }

    public function fetch(string $property)
    {
        return User::query()->where('id', $this->get('id'))->first()->{$property} ?? null;
    }

    protected function externalFields(): array
    {
        return [
            'custom_fields',
            'plain_password',
            'old_password',
            'new_password',
            'otp'
        ];
    }

    public function name()
    {
        return $this->get('name');
    }

    public function id()
    {
        return $this->get('id');
    }

    public function username()
    {
        return $this->get('username');
    }

    public function role()
    {
        return $this->get('role');
    }

    public function getPassword()
    {
        return DB::table(Table::$usersTable)->where('id', $this->{'id'})->value('password');
    }

    protected $guarded = ['id'];

    public function collectExternalFields(UserQueryHandler $userQueryHandler): void
    {
        foreach ($userQueryHandler->getFields() as $field) {
            switch ($field) {
                case "custom_fields":
                    $customFields = $this->getUserCustomFields();
                    $this->set('custom_fields', $customFields);
                    break;
                default:
                    break;
            }
        }
    }

    public function getUserCustomFields(): array
    {
        $data = [];

        $records = DB::table(Table::$customFieldValuesTable)->where([
            'user_id' => $this->get('id')
        ])->get();

        $role = $this->get('role');

        $group = null;

        if ($role == User::TEACHER_ROLE) {
            $group = 'teacher';
        }

        if ($role == User::GROUP_ROLE || $role == User::STUDENT_ROLE) {
            $group = 'student';
        }

        if (!$group) {
            return $data;
        }

        $customFieldStructs = DB::table(Table::$customFieldStructsTable)->where([
            'group' => $group
        ])->get();

        $mappedCustomFields = $customFieldStructs->pluck(null, 'id')->toArray();

        foreach ($records as $record) {
            $record = (array)$record;

            $customFieldStruct = (array)$mappedCustomFields[$record['custom_field_id']] ?? null;

            if (!$customFieldStruct) {
                continue;
            }

            $value = $record['value'];

            if ($customFieldStruct['is_multiple'] == 1) {
                $value = json_decode($value, 1);
            } else {
                if ($customFieldStruct['type'] == CustomField::TYPE_SELECT || $customFieldStruct['type'] == CustomField::TYPE_CHECKBOX) {
                    $value = (int) $value;
                }
            }

            $data[$customFieldStruct['name']] = $value;
        }

        return $data;
    }
}
