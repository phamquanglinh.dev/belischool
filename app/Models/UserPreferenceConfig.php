<?php

namespace App\Models;

use App\Traits\Entity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPreferenceConfig extends BaseModel
{
    use Entity;
}
