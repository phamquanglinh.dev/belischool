<?php

namespace App\Models;

use App\ObjectHelper\Table;
use App\ObjectHelper\UserQueryHandler;
use App\Traits\Entity;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Vocabulary extends BaseModel
{
    use Entity;

    protected $table;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->table = Table::$vocabularyTable;
    }

    public static function defineFillableFields(): array
    {
        return [
            'vocabulary_id',
            'name',
            'created_by',
            'vocabulary_cards',
            'vocabulary_relation',
            'vocabulary_count',
            'context'
        ];
    }

    public static function defineRetrieveFields(): array
    {
        return [
            'vocabulary_id',
            'name',
            'created_by',
            'vocabulary_cards',
            'vocabulary_relation',
            'vocabulary_count',
            'context'
        ];
    }

    protected function externalFields(): array
    {
        return [
            'vocabulary_cards',
            'vocabulary_relation',
            'vocabulary_count',
            'context'
        ];
    }

    public static function defineSaveableFields(): array
    {
        return [
            'vocabulary_id',
            'name',
            'created_by',
        ];
    }

    public function handleInteractiveUser(Builder &$builder, UserQueryHandler $userQueryHandler, int $branchScope = 0): void
    {
//        $builder->where(function ($wrapBuilder) use ($userQueryHandler) {
//            $wrapBuilder->whereExists(function ($builder) use ($userQueryHandler) {
//                $builder->selectRaw(DB::raw(1))->from(Table::$vocabularyRelationTable)
//                    ->where(Table::$vocabularyRelationTable . 'vocabulary_id', $userQueryHandler->getUserId())
//                    ->where(Table::$vocabularyRelationTable . '.v', $userQueryHandler->getUserId());
//            })->orWhere('created_by', $userQueryHandler->getUserId());
//        });
    }

    public function collectExternalFields(UserQueryHandler $userQueryHandler)
    {
        foreach ($userQueryHandler->getFields() as $field) {
            switch ($field) {
                case 'vocabulary_cards':
                    $vocabularyCards = $this->collectVocabularyCards();
                    $this->set('vocabulary_cards', $vocabularyCards);
                    break;
                default:
                    break;
            }
        }
    }

    public function collectVocabularyCards(): array
    {
        $records = DB::table(Table::$vocabularyCardTable)->where('vocabulary_id', $this->get('vocabulary_id'))
            ->orderBy('order')
            ->get([
                'vocabulary_card_id',
                'order',
                'vocabulary_id',
                'name',
                'translated',
                'system_voice',
                'teacher_voice',
                'teacher_image',
            ]);

        return $records->toArray();
    }
}
