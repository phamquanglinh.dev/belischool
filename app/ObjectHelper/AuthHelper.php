<?php

namespace App\ObjectHelper;

class AuthHelper
{
    protected const KEY = 'lantrancham';
    public static function encryptString($plaintext): string
    {
        $key = static::KEY;
        $cipher = "AES-128-CBC";
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipher)); // tạo vector khởi tạo (IV)
        $encrypted = openssl_encrypt($plaintext, $cipher, $key, 0, $iv); // mã hóa dữ liệu
        return base64_encode($encrypted . '::' . $iv);
    }

    public static function decryptString($encryptedString): bool|string
    {
        $key = static::KEY;
        $cipher = "AES-128-CBC";
        list($encrypted_data, $iv) = explode('::', base64_decode($encryptedString), 2);
        return openssl_decrypt($encrypted_data, $cipher, $key, 0, $iv);
    }
}
