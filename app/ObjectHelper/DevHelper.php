<?php

namespace App\ObjectHelper;

use Illuminate\Support\Str;

class DevHelper
{
    public static function toRawSql(string $sql, array $bindings): string
    {
        foreach ($bindings as $binding) {
            // Nếu giá trị ràng buộc là một chuỗi, thêm dấu ngoặc kép xung quanh
            if (is_string($binding)) {
                $binding = "'" . $binding . "'";
            }

            // Thay thế dấu chấm hỏi đầu tiên trong SQL bằng giá trị ràng buộc
            $sql = Str::replaceFirst('?', $binding, $sql);
        }

        return $sql;
    }
}
