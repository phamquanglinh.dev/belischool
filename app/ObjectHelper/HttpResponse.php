<?php

namespace App\ObjectHelper;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;

class HttpResponse
{
    public function responseModelCollection(Collection $collection, int $count = 0, bool $hasMore = false): JsonResponse
    {
        return response()->json([
            'data' => $collection->toArray(),
            'total' => $count,
            'has_more' => $hasMore
        ]);
    }

    public function responseDataModel(Model $model): JsonResponse
    {
        return response()->json([
            'data' => $model->toArray()
        ]);
    }

    public function responseData(array $data): JsonResponse
    {
        return response()->json($data);
    }

    public function responseDataCreated(int $id): JsonResponse
    {
        return response()->json([
            'message' => __('response.create.succeed'),
            'id' => $id
        ]);
    }

    public function responseDataUpdated(int $id): JsonResponse
    {
        return response()->json([
            'message' => __('response.update.succeed'),
            'id' => $id
        ]);
    }

    public function responseDataDeleted(): JsonResponse
    {
        return response()->json([
            'message' => __('response.delete.succeed'),
        ]);
    }
}
