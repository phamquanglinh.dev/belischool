<?php

namespace App\ObjectHelper;


use App\Events\User\UserCreated;
use App\Events\User\UserDeleted;
use App\Events\User\UserUpdated;
use App\Events\Vocabulary\VocabularyWasCreated;
use App\Events\Vocabulary\VocabularyWasDeleted;
use App\Events\Vocabulary\VocabularyWasUpdated;
use App\Models\BaseModel;
use App\Models\User;
use App\Models\Vocabulary;
use App\Traits\Entity;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Throwable;

class QueryHelper
{
    /**
     * Lấy tất cả các bản ghi theo `UserQueryHandler`.
     *
     * * @param class-string<T> $modelClass
     * @param UserQueryHandler $userQueryHandler
     *
     * @return Collection<int, T> Danh sách các model trong collection
     * @template T of object
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since: 25/10/2024 16:43:09
     */
    public static function handleGetAll(string $modelClass, UserQueryHandler $userQueryHandler): Collection
    {
        $builder = (new $modelClass())->handleBuildQuery($userQueryHandler);

        /**
         * @var Collection $collection
         */
        $collection = $builder->get();

        $collection->each(function ($model) use ($userQueryHandler) {
            $model->collectExternalFields($userQueryHandler);
//            $model->handleCastData($userQueryHandler);
        });


        return $collection;
    }

    /**
     * @param class-string<T> $modelClass
     * @param UserQueryHandler $userQueryHandler
     *
     * @return T|null
     * @template T of object
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  25/10/2024 16:42:38
     */
    public static function handleGetOne(string $modelClass, UserQueryHandler $userQueryHandler): ?BaseModel
    {
        /**
         * @see Entity::handleBuildQuery()
         * @var Builder $builder
         */
        $builder = (new $modelClass())->handleBuildQuery($userQueryHandler);

        /**
         * @var BaseModel $model
         */
        $model = $builder->first();

        if (!$model) {
            return null;
        }

        $model->collectExternalFields($userQueryHandler);

        return $model;
    }

    /**
     * @param class-string<T> $modelClass
     * @param int $id
     * @param int $userId
     * @return T|null
     * @template T of object
     */
    public static function find(string $modelClass, int $id, int $userId)
    {
        $userQueryHandler = new UserQueryHandler([
            'where' => [
                'id' => $id
            ]
        ], $modelClass);

        $userQueryHandler->setUserId($userId);

        return static::handleGetOne($modelClass, $userQueryHandler, 0);
    }

    public static function handleCount(string $modelClass, UserQueryHandler $userQueryHandler): int
    {
        $builder = (new $modelClass())->handleBuildQueryCount($userQueryHandler);

        return $builder->count();
    }

    /**
     * @param BaseModel $model
     *
     * @return BaseModel
     *
     * @throws Throwable
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  25/10/2024 16:45:50
     */
    public static function handleCreate(BaseModel $model): BaseModel
    {
        [$primaryFields, $externalFields] = $model->handlePrimaryData($model->getAttributes());

        $model->setRawAttributes($primaryFields);

        DB::beginTransaction();

        return DB::transaction(function () use ($externalFields, $model) {
            $model->saveOrFail();

            $model->populate($externalFields);

            $events = self::mappedEvents()[$model::class]['created'] ?? [];

            if (!empty($events)) {
                foreach ($events as $event) {
                    event(new $event($model));
                }
            }

            DB::commit();

            return $model;
        });
    }

    public static function handleUpdate(BaseModel $model): bool
    {
        [$primaryFields, $externalFields] = $model->handlePrimaryData($model->getAttributes());

        $model->setRawAttributes($primaryFields);

        $model->save();

        $model->populate($externalFields);

        $events = self::mappedEvents()[$model::class]['updated'] ?? [];

        if (!empty($events)) {
            foreach ($events as $event) {
                event(new $event($model));
            }
        }

        return true;
    }

    public static function handleDelete(BaseModel $model): bool
    {
        $model->delete();

        $events = self::mappedEvents()[$model::class]['deleted'] ?? [];

        if (!empty($events)) {
            foreach ($events as $event) {
                event(new $event($model));
            }
        }

        return true;
    }

    public function handleGetModelById(BaseModel $model, int $id): ?Model
    {
        $record = $model->newQuery()->where('id', $id)->first();

        if (!$record) {
            return null;
        }

        return $record;
    }

    private static function mappedEvents(): array
    {
        return [
            User::class => [
                'created' => [UserCreated::class],
                'updated' => [UserUpdated::class],
                'deleted' => [UserDeleted::class]
            ],
            Vocabulary::class => [
                'created' => [VocabularyWasCreated::class],
                'updated' => [VocabularyWasUpdated::class],
                'deleted' => [VocabularyWasDeleted::class],
            ]
        ];
    }
}
