<?php

namespace App\ObjectHelper;

class Table
{
    public static string $notificationTable = 'notifications';
    public static string $usersTable = 'users';
    public static string $notificationUserTable = 'notification_user';
    public static string $customFieldStructsTable = 'custom_field_structs';
    public static string $customFieldListsTable = 'custom_field_lists';
    public static string $customFieldValuesTable = 'custom_field_values';
    public static string $passwordResetTable = 'password_resets';

    public static string $otp = 'otp';
    public static string $vocabularyTable = 'vocabulary';
    public static string $vocabularyCardTable = 'vocabulary_card';
    public static string $vocabularyRelationTable = 'vocabulary_relation';
    public static string $vocabularyLearnLogTable = 'vocabulary_learn_log';

}
