<?php

namespace App\ObjectHelper;

class UserQueryHandler
{
    private array $filtering = [];
    private string $fields = '';

    private int $limit = 20;
    private int $offset = 0;

    private string $direction = 'DESC';
    private array $conditions = [];
    private string $order = 'created_at';

    private string $branch = '';
    private int $userId = 0;

    private bool $interactiveScope = true;

    public function getRole(): int
    {
        return $this->role;
    }

    public function setOffset(int $offset): void
    {
        $this->offset = $offset;
    }

    public function setRole(int $role): void
    {
        $this->role = $role;
    }

    public function setWhere(array $where): void
    {
        $this->where = $where;

        foreach ($this->where as $key => $conditionValue) {
            $parseKey = explode(":", $key);

            $this->addCondition($this->parseCondition([
                'field' => $parseKey[0],
                'operator' => $parseKey[1] ?? "eq",
                'value' => $conditionValue
            ]));
        }
    }

    private int $role = 0;

    private array $where = [];

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getBranch(): string
    {
        return $this->branch;
    }

    /**
     * @param array $data
     * @param string|null $model
     */
    public function __construct(array $data, string $model = null)
    {
        foreach ($data as $field => $value) {
            if (property_exists($this, $field)) {
                $this->$field = $value;
            }
        }

        if (empty($this->fields)) {
            $defaultRetrieveData = $model ? $model::defineRetrieveFields() : [];

            $this->fields = implode(',', [...$defaultRetrieveData, 'id']);
        }

        if (!empty($this->filtering)) {
            foreach ($this->filtering as $condition => $value) {
                $conditionParsed = explode(':', $condition);

                $field = $conditionParsed[0];
                $operator = $conditionParsed[1] ?? "=";

                $this->addCondition($this->parseCondition([
                    'field' => $field,
                    'operator' => $operator,
                    'value' => $value
                ]));
            }
        }

        if (!empty($this->where)) {
            foreach ($this->where as $key => $conditionValue) {
                $parseKey = explode(":", $key);

                $this->addCondition($this->parseCondition([
                    'field' => $parseKey[0],
                    'operator' => $parseKey[1] ?? "eq",
                    'value' => $conditionValue
                ]));
            }
        }
    }

    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    public function setBranch(string $branch): void
    {
        $this->branch = $branch;
    }

    public function getConditions(): array
    {
        return $this->conditions;
    }

    public function setConditions(array $conditions): void
    {
        $this->conditions = $conditions;
    }

    public function addCondition(array $condition): void
    {
        $this->conditions[] = $condition;
    }

    public function getFields(): array
    {
        return explode(',', $this->fields);
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getDirection(): string
    {
        return $this->direction;
    }

    public function getOrder(): string
    {
        return $this->order;
    }

    private function parseCondition(array $condition): array
    {
        if (!$condition['operator']) {
            $condition['operator'] = '=';
        }

        switch ($condition['operator']) {
            case "eq":
                $condition['operator'] = "=";
                break;
            case "ne":
                $condition['operator'] = "!=";
                break;
            case "lt":
                $condition['operator'] = "<";
                break;
            case "lte":
                $condition['operator'] = "<=";
                break;
            case "gt":
                $condition['operator'] = ">";
                break;
            case "gte":
                $condition['operator'] = ">=";
                break;
            case "contain":
                $condition['operator'] = "LIKE";
                $condition['value'] = "%" . $condition['value'] . "%";
                break;
            default:
                break;
        }

        return $condition;
    }

    public function setFields(array $fields): void
    {
        $this->fields = implode(',', $fields);
    }

    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    public function setOrder(string $order): void
    {
        $this->order = $order;
    }

    public function setDirection(string $direction): void
    {
        $this->direction = $direction;
    }

    public function hasCondition(string $conditionName): bool
    {
        return in_array($conditionName, array_keys($this->conditions));
    }

    public function getCondition(string $conditionName): ?string
    {
        foreach ($this->conditions as $condition) {
            if ($condition['field'] === $conditionName) {
                return $condition['value'];
            }
        }
        return null;
    }

    public function disableInteractiveScopes(): void
    {
        $this->interactiveScope = false;
    }

    public function interactiveScope(): bool
    {
        return $this->interactiveScope;
    }
}
