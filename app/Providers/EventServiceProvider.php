<?php

namespace App\Providers;

use App\EventListeners\User\ForgetPasswordUpdatedThenSendForgotPasswordMail;
use App\EventListeners\User\UserCreatedThenSendWelcomeEmail;
use App\EventListeners\User\UserRequestVerifiedThenSendVerifySmsOrEmail;
use App\EventListeners\User\UserUpdatedThenCreateProfileUserIfUserIsGroupUserType;
use App\EventListeners\User\UserUpdatedThenSaveCustomFields;
use App\EventListeners\Vocabulary\VocabularyWasCreatedThenSaveVocabularyCards;
use App\Events\User\ForgetPasswordWasUpdated;
use App\Events\User\UserCreated;
use App\Events\User\UserRequestVerified;
use App\Events\User\UserUpdated;
use App\Events\Vocabulary\VocabularyWasCreated;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        UserUpdated::class => [
            UserUpdatedThenCreateProfileUserIfUserIsGroupUserType::class,
            UserUpdatedThenSaveCustomFields::class
        ],
        UserCreated::class => [
            UserCreatedThenSendWelcomeEmail::class
        ],
        ForgetPasswordWasUpdated::class => [
            ForgetPasswordUpdatedThenSendForgotPasswordMail::class
        ],
        UserRequestVerified::class => [
            UserRequestVerifiedThenSendVerifySmsOrEmail::class
        ],
        VocabularyWasCreated::class => [
            VocabularyWasCreatedThenSaveVocabularyCards::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
