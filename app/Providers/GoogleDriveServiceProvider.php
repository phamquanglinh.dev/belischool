<?php

use Google\Client as GoogleClient;
use Google\Service\Drive as GoogleDrive;
use Illuminate\Support\ServiceProvider;
use League\Flysystem\Filesystem;

class GoogleDriveServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->singleton('google.drive', function ($app) {
            $client = new GoogleClient();
            $client->setClientId(config('filesystems.disks.google.client_id'));
            $client->setClientSecret(config('filesystems.disks.google.client_secret'));
            $client->refreshToken(config('filesystems.disks.google.refresh_token'));

            $service = new GoogleDrive($client);

            return new Filesystem(new GoogleDriveAdapter($service, config('filesystems.disks.google.folder_id')));
        });
    }
}

