<?php

namespace App\Services;

use App\Models\User;
use App\ObjectHelper\AuthHelper;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;

class AuthenticationService
{
    protected Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getCurrentUser(): ?User
    {
        try {
            $token = $this->request->bearerToken();  // Sử dụng instance của request

            $decoded = json_decode(AuthHelper::decryptString($token));

            /**
             * @var User $user
             */
            $user = User::query()->find($decoded->id);

            return $user;
        } catch (Exception $e) {
            return null;
        }
    }
}
