<?php
namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

class DropboxService
{
    protected Client $client;
    protected string $accessToken;

    public function __construct()
    {
        $this->client = new Client();
        $this->accessToken = env('DROPBOX_ACCESS_TOKEN');
    }

    // Upload file lên Dropbox
    public function uploadFile($path, $fileContents)
    {
        $url = 'https://content.dropboxapi.com/2/files/upload';
        $response = $this->client->post($url, [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->accessToken,
                'Content-Type' => 'application/octet-stream',
                'Dropbox-API-Arg' => json_encode([
                    'path' => $path,
                    'mode' => 'add',
                    'autorename' => true,
                    'mute' => false,
                ]),
            ],
            'body' => $fileContents,
        ]);

        return json_decode($response->getBody(), true);
    }

    // Lấy thông tin file từ Dropbox

    /**
     * @throws GuzzleException
     */
    public function getFile($path)
    {
        try {
            $response = $this->client->get('https://content.dropboxapi.com/2/files/download', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->accessToken,
                    'Dropbox-API-Arg' => json_encode(['path' => '/' . $path]),
                ],
            ]);

            // Lấy nội dung file
            $fileContents = $response->getBody()->getContents();
            $fileName = basename($path);
            $contentType = $response->getHeader('Content-Type')[0] ?? 'application/octet-stream';

            return [
                'contents' => $fileContents,
                'name' => $fileName,
                'type' => $contentType,
            ];
        } catch (RequestException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    // Liệt kê các file trong thư mục Dropbox
    public function listFiles($path = '')
    {
        $url = 'https://api.dropboxapi.com/2/files/list_folder';
        $response = $this->client->post($url, [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->accessToken,
                'Content-Type' => 'application/json',
            ],
            'json' => ['path' => $path],
        ]);

        return json_decode($response->getBody(), true);
    }
}
