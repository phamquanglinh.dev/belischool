<?php

namespace App\Services;

use Google\Client;
use Google\Service\Gmail;

class GmailService
{
    protected $client;
    protected $gmailService;

    public function __construct()
    {
        $this->client = new Client();
        $this->client->setAuthConfig(storage_path('app/credentials.json'));
        $this->client->addScope(Gmail::GMAIL_SEND);
        $this->client->setAccessType('offline');

        // Kiểm tra và làm mới access token nếu cần
        if ($this->client->isAccessTokenExpired()) {
            $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
        }

        $this->gmailService = new Gmail($this->client);
    }

    public function sendMail($toEmail, $subject, $body): void
    {
        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom('your-email@gmail.com')
            ->setTo($toEmail)
            ->setBody($body);

        $mime = $message->toString();
        $encodedMessage = base64_encode($mime);

        $message = new Gmail\Message();
        $message->setRaw($encodedMessage);

        $this->gmailService->users_messages->send('me', $message);
    }
}
