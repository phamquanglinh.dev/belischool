<?php

namespace App\Services;

use GuzzleHttp\Exception\GuzzleException;
use Microsoft\Graph\Exception\GraphException;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use Illuminate\Support\Facades\Http;

class MailService
{
    protected Graph $graph;

    public function __construct()
    {
        $this->graph = new Graph();
        $this->graph->setAccessToken($this->getAccessToken());
    }

    protected function getAccessToken(): string
    {
        $response = Http::asForm()->post('https://login.microsoftonline.com/' . env('MS_GRAPH_TENANT_ID') . '/oauth2/v2.0/token', [
            'client_id' => env('MS_GRAPH_CLIENT_ID'),
            'client_secret' => env('MS_GRAPH_CLIENT_SECRET'),
            'scope' => 'https://graph.microsoft.com/.default',
            'grant_type' => 'client_credentials',
        ]);

        return $response->json()['access_token'];
    }

    /**
     * @throws GuzzleException
     * @throws GraphException
     */
    public function sendMail($toEmail, $subject, $body)
    {
        $message = [
            "message" => [
                "subject" => $subject,
                "body" => [
                    "contentType" => "Text",
                    "content" => $body
                ],
                "toRecipients" => [
                    [
                        "emailAddress" => [
                            "address" => $toEmail
                        ]
                    ]
                ]
            ]
        ];

        $this->graph->createRequest("POST", "/users/{your_email}/sendMail")
            ->attachBody($message)
            ->execute();
    }
}
