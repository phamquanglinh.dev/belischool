<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 06/09/2024 10:43 am
 */

namespace App\Services;

use App\Factories\NotificationFactory;
use App\Models\AppNotification;
use App\Models\User;
use App\ObjectHelper\QueryHelper;

class NotificationService
{
    private NotificationFactory $notificationFactory;

    public function __construct(
        NotificationFactory $notificationFactory
    )
    {
        $this->notificationFactory = $notificationFactory;
    }

    /**
     * @throws \Throwable
     */
    public function createNotification(
        string $actionType,
        string $objectType,
        int $objectId,
        string $branch,
        array $objectData,
        array $interactiveUser
    ): void
    {
        $notificationData = [
            'action_type' => $actionType,
            'object_type' => $objectType,
            'object_id' => $objectId,
            'branch' => $branch,
            'object_data' => $objectData
        ];

        $notification = $this->notificationFactory->makeNotification($notificationData, $interactiveUser);

        QueryHelper::handleCreate($notification);
    }
}
