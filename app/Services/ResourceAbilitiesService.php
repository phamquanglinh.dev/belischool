<?php

namespace App\Services;

use App\Models\Comment;
use App\Models\ImportSetting;
use App\Models\Role;
use App\Models\Salary\SalaryCriteria;
use App\Models\StudyLog;
use App\Models\Transaction;
use App\Models\User;
use App\ObjectHelper\AclModule;
use App\ObjectHelper\AclPermission;
use App\ObjectHelper\QueryHelper;
use Illuminate\Support\Str;

class ResourceAbilitiesService
{
    public function canCreateVocabulary(int $userId): bool
    {
        $user = QueryHelper::find(User::class, $userId, 0);

        if ($user->role() == User::TEACHER_ROLE) {
            return true;
        }

        return false;
    }
}
