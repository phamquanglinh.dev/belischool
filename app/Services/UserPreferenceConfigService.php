<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 21/08/2024 4:23 pm
 */

namespace App\Services;

use App\Models\UserPreferenceConfig;

class UserPreferenceConfigService
{
    public function getUserConfig(string $type, $userId): ?UserPreferenceConfig
    {
        /**
         * @var UserPreferenceConfig $config
         */
        $config = UserPreferenceConfig::query()->where('user_id', $userId)->where('type', $type)->first()?->settings;

        if (! $config) {
            return null;
        }

        return $config;
    }

    public function getGlobalConfig() {}

    public function setUserConfig(string $type, int $userId, string $content): bool
    {
        /**
         * @var UserPreferenceConfig $config
         */
        $config = UserPreferenceConfig::query()->updateOrCreate([
            'type' => $type,
            'user_id' => $userId
        ],[
            'type' => $type,
            'user_id' => $userId,
            'settings' => $content
        ]);

        return true;
    }
}
