<?php

namespace App\Traits;

use App\ObjectHelper\DevHelper;
use App\ObjectHelper\UserQueryHandler;
use App\Services\ResourceAbilitiesService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

trait Entity
{
    protected function getVisibleFields(): array
    {
        return [];
    }

    protected function externalFields(): array
    {
        return [];
    }

    public static function defineRetrieveFields(): array
    {
        return [];
    }

    public static function defineSaveableFields(): array
    {
        return [];
    }

    public static function defineFillableFields(): array
    {
        return [];
    }

    public static function defineIntFields(): array
    {
        return [];
    }

    public function collectExternalFields(UserQueryHandler $userQueryHandler)
    {
//        foreach ($userQueryHandler->getFields() as $field) {
//            switch ($field) {
//                default:
//                    break;
//            }
//        }
    }

    public function handleBuildQuery(UserQueryHandler $userQueryHandler): Builder
    {
        $builder = self::query();

        $fields = [];

        foreach ($userQueryHandler->getFields() as $field) {
            if (!in_array($field, $this->externalFields())) {
                $fields[] = $field;
            }

            if (!empty(self::defineRetrieveFields())) {
                if (!in_array($field, self::defineRetrieveFields())) {
                    throw new BadRequestException("$field not allow get. Available fields : " . implode(',', self::defineRetrieveFields()));
                }
            }
        }

        $this->handleFields($builder, $fields, $userQueryHandler);

        $this->handleCondition($builder, $userQueryHandler);

        if ($userQueryHandler->interactiveScope()) {
            $this->handleInteractiveUser($builder, $userQueryHandler);
        }

        $builder->orderBy($userQueryHandler->getOrder(), $userQueryHandler->getDirection());

        $builder->limit($userQueryHandler->getLimit());

        $builder->offset($userQueryHandler->getOffset());

        return $builder;
    }

    public function handleBuildQueryCount(UserQueryHandler $userQueryHandler, int $branchScope = 0): Builder
    {
        $builder = self::query();

        $fields = [];

        foreach ($userQueryHandler->getFields() as $field) {
            if (!in_array($field, $this->externalFields())) {
                $fields[] = $field;
            }

            if (!empty(self::defineRetrieveFields())) {
                if (!in_array($field, self::defineRetrieveFields())) {
                    throw new BadRequestException("$field not allow get. Available fields : " . implode(',', self::defineRetrieveFields()));
                }
            }
        }

        $builder->select($fields);

        $this->handleCondition($builder, $userQueryHandler, $branchScope);

        if ($userQueryHandler->interactiveScope()) {
            $this->handleInteractiveUser($builder, $userQueryHandler, $branchScope);
        }

        return $builder;
    }

    protected function handleCondition(Builder &$builder, UserQueryHandler $userQueryHandler, int $branchScope = 0): void
    {
        foreach ($userQueryHandler->getConditions() as $condition) {
            $builder->where($condition['field'], $condition['operator'], $condition['value']);
        }

        if ($branchScope === 1) {
            $builder->where('branch', $userQueryHandler->getBranch());
        }
    }

    protected function handleInteractiveUser(Builder &$builder, UserQueryHandler $userQueryHandler, int $branchScope = 0): void
    {
//        /**
//         * @var $resourceAbilitiesService
//         */
//        $resourceAbilitiesService = app(ResourceAbilitiesService::class);
    }

    protected function handleFields(Builder &$builder, array $fields, UserQueryHandler $userQueryHandler): void
    {
        $builder->select($fields);
    }

    public function populate(array $data = []): void
    {
        foreach ($data as $key => $value) {
            if (!empty(static::defineFillableFields())) {
                if (!in_array($key, static::defineFillableFields())) {
                    continue;
                }
            }

            $this->$key = $value;
        }
    }

    public function handlePrimaryData(array $data): array
    {
        $primaryData = [];

        $externalData = [];

        foreach ($data as $key => $value) {
            if (!in_array($key, $this->externalFields())) {
                $primaryData[$key] = $value;
            } else {
                $externalData[$key] = $value;
            }
        }

        return [$primaryData, $externalData];
    }
}
