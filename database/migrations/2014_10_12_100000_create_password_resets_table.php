<?php

use App\ObjectHelper\Table;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePasswordResetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Table::$passwordResetTable, function (Blueprint $table) {
            $table->string('username')->unique();
            $table->string('token');
            $table->bigInteger('expired_in')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Table::$passwordResetTable);
    }
}
