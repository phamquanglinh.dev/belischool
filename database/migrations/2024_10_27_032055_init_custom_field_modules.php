<?php

use App\ObjectHelper\Table;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InitCustomFieldModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable(Table::$customFieldStructsTable)) {
            Schema::create(Table::$customFieldStructsTable, function (Blueprint $table) {
               $table->id();
               $table->string('name')->unique();
               $table->string('type');
               $table->string('description');
               $table->integer('is_multiple')->nullable();
               $table->integer('is_required')->nullable();
               $table->integer('is_unique')->nullable();
               $table->integer('is_default')->nullable();
               $table->string('group')->nullable();
            });
        }

        if (! Schema::hasTable(Table::$customFieldListsTable)) {
            Schema::create(Table::$customFieldListsTable, function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->integer('custom_field_id');
            });
        }

        if (! Schema::hasTable(Table::$customFieldValuesTable)) {
            Schema::create(Table::$customFieldValuesTable, function (Blueprint $table) {
                $table->id();
                $table->integer('custom_field_id');
                $table->integer('user_id');
                $table->text('value')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Table::$customFieldStructsTable);
        Schema::dropIfExists(Table::$customFieldListsTable);
        Schema::dropIfExists(Table::$customFieldValuesTable);
    }
}
