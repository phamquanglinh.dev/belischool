<?php

use App\ObjectHelper\Table;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable(Table::$vocabularyTable)) {
            Schema::create(Table::$vocabularyTable, function (Blueprint $table) {
                $table->id('vocabulary_id');
                $table->string('name');
                $table->integer('created_by')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }

        if (!Schema::hasTable(Table::$vocabularyCardTable)) {
            Schema::create(Table::$vocabularyCardTable, function (Blueprint $table) {
                $table->id('vocabulary_card_id');
                $table->integer('order');
                $table->integer('vocabulary_id');
                $table->string('name');
                $table->string('translated');
                $table->string('system_voice');
                $table->string('teacher_voice')->nullable();
                $table->string('teacher_image');
                $table->integer('created_by')->nullable();
                $table->softDeletes();
                $table->timestamps();
                $table->index(['vocabulary_id'], 'vocabulary_id_index');
            });
        }

        if (!Schema::hasTable(Table::$vocabularyRelationTable)) {
            Schema::create(Table::$vocabularyRelationTable, function (Blueprint $table) {
                $table->id();
                $table->integer('vocabulary_id');
                $table->integer('user_id');
                $table->timestamps();
            });
        }

        if (!Schema::hasTable(Table::$vocabularyLearnLogTable)) {
            Schema::create(Table::$vocabularyLearnLogTable, function (Blueprint $table) {
                $table->id();
                $table->integer('vocabulary_id');
                $table->integer('vocabulary_card_id');
                $table->integer('user_id');
                $table->string('custom_image');
                $table->string('custom_sentence');
                $table->string('pronunciation_point');
                $table->unique(['vocabulary_id', 'vocabulary_card_id', 'user_id'], 'unique_learned_vocabulary');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists(Table::$vocabularyTable);
        Schema::dropIfExists(Table::$vocabularyCardTable);
        Schema::dropIfExists(Table::$vocabularyRelationTable);
        Schema::dropIfExists(Table::$vocabularyLearnLogTable);
    }
};
