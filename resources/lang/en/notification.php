<?php

return [
    'act_create' => [
        'student' => [
            'title' => 'Thêm mới học sinh',
            'description' => ':name đã tạo học sinh :student_name'
        ],
    ],
    'act_update' => [
        'student' => [
            'title' => 'Chỉnh sửa học sinh',
            'description' => ':name đã chỉnh sửa thông tin học sinh :student_name'
        ]
    ],
    'act_approve' => [
        'card' => [
            'title' => 'Giao dịch được duyệt',
            'description' => ':name đã duyệt giao dịch :uuid'
        ]
    ]
];
