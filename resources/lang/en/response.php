<?php

return [
    'create' => [
        'succeed' => 'Thêm mới thành công'
    ],
    'update' => [
        'succeed' => 'Cập nhật thành công'
    ],
    'delete' => [
        'succeed' => 'Xoá thành công'
    ],
    'sync' => [
        'succeed' => 'Đồng bộ thành công'
    ],
    'validation' => 'Vui lòng thử lại',
    'not_found' => 'Không tìm thấy',
    'not_found_route' => 'Không tìm thấy (route)',
    'unauthorized' => 'Unauthorized',
    'forbidden' => 'Bạn không có quyền thực hiện',
    'send' => [
        'succeed' => 'Gửi thành công',
        'failed' => 'Gửi thất bại',
    ]
];
