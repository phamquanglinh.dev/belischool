<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Yêu cầu khôi phục mật khẩu</title>
    <style>
        /* Định dạng cơ bản */
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f4f8;
            color: #333;
            margin: 0;
            padding: 0;
        }
        .container {
            max-width: 600px;
            margin: 40px auto;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 4px 12px rgba(0, 0, 0, 0.1);
            overflow: hidden;
        }
        .header {
            background-color: #28a745; /* Màu xanh lá */
            padding: 20px;
            text-align: center;
            color: #fff;
        }
        .header h1 {
            font-size: 24px;
            margin: 0;
        }
        .content {
            padding: 20px;
        }
        .content p {
            font-size: 16px;
            line-height: 1.6;
        }
        .content h2 {
            color: #28a745; /* Màu xanh lá */
            font-size: 20px;
            margin-top: 0;
        }
        .button {
            display: inline-block;
            margin-top: 20px;
            padding: 12px 24px;
            color: #ffffff; /* Màu chữ trắng */
            background-color: #28a745; /* Màu xanh lá */
            border-radius: 5px;
            text-decoration: none;
            font-weight: bold;
            font-size: 16px;
            transition: background-color 0.3s ease;
        }
        .button:hover {
            background-color: #218838; /* Màu xanh lá đậm hơn */
        }
        .footer {
            padding: 15px;
            background-color: #f9fafc;
            text-align: center;
            font-size: 14px;
            color: #777;
        }
    </style>
</head>
<body>
<div class="container">
    <!-- Header -->
    <div class="header">
        <h1>Yêu cầu khôi phục mật khẩu</h1>
    </div>

    <!-- Content -->
    <div class="content">
        <h2>Xin chào, {{ $name }}!</h2>
        <p>Bạn vừa yêu cầu khôi phục mật khẩu cho tài khoản của mình. Để đặt lại mật khẩu, vui lòng nhấn vào nút bên dưới:</p>

        <!-- Password Reset Button -->
        <a href="{{ $forgetLink }}" class="button">Đặt lại mật khẩu</a>

        <p>Nếu bạn không yêu cầu khôi phục mật khẩu, vui lòng bỏ qua email này hoặc liên hệ với bộ phận hỗ trợ để được giúp đỡ.</p>
    </div>

    <!-- Footer -->
    <div class="footer">
        <p>&copy; {{ date('Y') }} {{ config('app.name') }}. Mọi quyền được bảo lưu.</p>
    </div>
</div>
</body>
</html>
