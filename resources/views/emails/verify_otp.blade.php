<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác minh OTP</title>
    <style>
        /* Định dạng cơ bản */
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f4f8;
            color: #333;
            margin: 0;
            padding: 0;
        }
        .container {
            max-width: 600px;
            margin: 40px auto;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 4px 12px rgba(0, 0, 0, 0.1);
            overflow: hidden;
        }
        .header {
            background-color: #28a745; /* Màu xanh lá */
            padding: 20px;
            text-align: center;
            color: #fff;
        }
        .header h1 {
            font-size: 24px;
            margin: 0;
        }
        .content {
            padding: 20px;
        }
        .content p {
            font-size: 16px;
            line-height: 1.6;
        }
        .content h2 {
            color: #28a745; /* Màu xanh lá */
            font-size: 20px;
            margin-top: 0;
        }
        .otp {
            display: inline-block;
            font-size: 24px;
            font-weight: bold;
            color: #ffffff;
            background-color: #28a745;
            padding: 10px 20px;
            border-radius: 5px;
            text-align: center;
            margin-top: 20px;
            letter-spacing: 2px;
        }
        .footer {
            padding: 15px;
            background-color: #f9fafc;
            text-align: center;
            font-size: 14px;
            color: #777;
        }
    </style>
</head>
<body>
<div class="container">
    <!-- Header -->
    <div class="header">
        <h1>Xác minh tài khoản của bạn</h1>
    </div>

    <!-- Content -->
    <div class="content">
        <h2>Xin chào, {{ $user->get('name') }}!</h2>
        <p>Cảm ơn bạn đã đăng ký tài khoản với chúng tôi. Để hoàn tất quá trình đăng ký, vui lòng sử dụng mã OTP bên dưới để xác minh tài khoản của bạn:</p>

        <!-- OTP Code -->
        <div class="otp">{{ $otp }}</div>

        <p>Mã OTP sẽ hết hạn sau 40 phút. Nếu bạn không yêu cầu xác minh tài khoản, vui lòng bỏ qua email này hoặc liên hệ với bộ phận hỗ trợ.</p>
    </div>

    <!-- Footer -->
    <div class="footer">
        <p>&copy; {{ date('Y') }} {{ config('app.name') }}. Mọi quyền được bảo lưu.</p>
    </div>
</div>
</body>
</html>
