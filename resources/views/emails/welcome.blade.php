<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chào mừng bạn đến với dịch vụ của chúng tôi!</title>
    <style>
        /* Định dạng chung */
        body {
            font-family: Arial, sans-serif;
            color: #333;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        .container {
            max-width: 600px;
            margin: 20px auto;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            overflow: hidden;
        }
        .header {
            background-color: #4CAF50;
            padding: 20px;
            text-align: center;
            color: #fff;
        }
        .header h1 {
            font-size: 24px;
            margin: 0;
        }
        .content {
            padding: 20px;
        }
        .content p {
            font-size: 16px;
            line-height: 1.5;
        }
        .content h2 {
            color: #4CAF50;
            font-size: 20px;
            margin-bottom: 10px;
        }
        .button {
            display: inline-block;
            padding: 12px 20px;
            color: #fff;
            background-color: #4CAF50;
            border-radius: 5px;
            text-decoration: none;
            font-size: 16px;
            font-weight: bold;
            margin-top: 20px;
        }
        .footer {
            background-color: #eee;
            padding: 15px;
            text-align: center;
            font-size: 14px;
            color: #777;
        }
    </style>
</head>
<body>
<div class="container">
    <!-- Header -->
    <div class="header">
        <h1>Chào mừng bạn đến với {{ config('app.name') }}!</h1>
    </div>

    <!-- Content -->
    <div class="content">
        <h2>Xin chào, {{ $name }}!</h2>
        <p>Chúng tôi rất vui mừng khi bạn gia nhập cộng đồng của {{ config('app.name') }}. Đội ngũ của chúng tôi luôn nỗ lực để mang đến cho bạn trải nghiệm tuyệt vời nhất.</p>
        <p>Hãy sẵn sàng khám phá và tận hưởng những tính năng độc đáo mà chúng tôi đã chuẩn bị cho bạn!</p>

        <p>Nếu có bất kỳ câu hỏi nào, đừng ngần ngại liên hệ với đội ngũ hỗ trợ của chúng tôi. Chúng tôi luôn sẵn sàng giúp đỡ bạn!</p>

        <!-- Button to go to website -->
        <a href="{{ url('/') }}" class="button">Khám phá ngay</a>
    </div>

    <!-- Footer -->
    <div class="footer">
        <p>&copy; {{ date('Y') }} {{ config('app.name') }}. Mọi quyền được bảo lưu.</p>
        <p>Bạn nhận được email này vì đã đăng ký tài khoản tại {{ config('app.name') }}.</p>
    </div>
</div>
</body>
</html>
