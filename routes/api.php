<?php

use App\Http\Controllers\Auth\AuthenticateController;
use App\Http\Controllers\Auth\CurrentUserController;
use App\Http\Controllers\Common\UploadController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\Vocabulary\VocabularyController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthenticateController::class, 'registerAction'])->withoutMiddleware(['beli']);
Route::post('login', [AuthenticateController::class, 'loginAction'])->withoutMiddleware(['beli']);
Route::post('find', [AuthenticateController::class, 'findAccountAction'])->withoutMiddleware(['beli']);
Route::post('forgot_password', [AuthenticateController::class, 'forgotPasswordAction'])->withoutMiddleware(['beli']);
Route::put('recover_password', [AuthenticateController::class, 'recoverPasswordAction'])->withoutMiddleware(['beli']);
Route::post('dropbox/upload', [UploadController::class, 'uploadActionViaDropbox'])->withoutMiddleware(['beli']);
Route::get('dropbox/file/{path}', [UploadController::class, 'getFileActionViaDropbox', 'path'])->withoutMiddleware(['beli']);
Route::post('s3/upload/', [FileController::class, 'upload'])->withoutMiddleware(['beli']);
Route::get('s3/file/{filename}', [FileController::class, 'downloadFile', 'filename'])->withoutMiddleware(['beli']);

Route::get('user', [AuthenticateController::class, 'getCurrentUserAction']);
Route::put('user', [CurrentUserController::class, 'updateCurrentUserAction']);
Route::post('require_verified', [AuthenticateController::class, 'requireVerifiedAction']);
Route::put('user/verify', [AuthenticateController::class, 'userVerifyAction']);
Route::get('user/profiles', [CurrentUserController::class, 'getAllUserProfilesAction']);
Route::post('user/profiles', [CurrentUserController::class, 'createUserProfilesAction']);
Route::put('user/profiles/{id}', [CurrentUserController::class, 'selectProfileAction']);


Route::prefix('vocabulary')->group(function () {
    Route::get('', [VocabularyController::class, 'getAllVocabularyAction']);
    Route::post('', [VocabularyController::class, 'createVocabularyAction']);
});
